const os = require('os');
const path = require('path');

let supportedPlatforms = {
  'linux': 'Linux',
  'win32': 'Windows',
  'darwin': 'macOS'
};

const darwinDefaultCompany = 'com.somi.sk';
const defaultAppDirName = 'Kerber Backup Client';
const kbcPath = function (platform = os.platform(), ...appName) {

  // need this 'cose win32 tests fail on linux => path.sep 
  const sep = platform.win32 ? path.win32.sep : path.posix.sep;
  const ret = {
    linux: appName || defaultAppDirName,
    darwin: appName.length < 2 ? [darwinDefaultCompany, appName] : appName,
    win32: appName || defaultAppDirName
  };

  return ret[platform].join(sep);

};

function getPlatformDirectoryName(platform = os.platform(), ...appName) {
  return kbcPath(platform, ...appName);
}

/**
 * Get users config path on windows
 *  
 * @param {String} username 
 * @param {String} platform - Default value is autodetected
 * 
 * @returns {String} Users configuration path
 */
function getConfigDirectory(username, appName = defaultAppDirName,
  platform = os.platform()) {
  let dispatcher = {
    darwin: (u, a) => {
      return _darwinConfigDirectory(u, a);
    },
    linux: (u, a) => {
      return _linuxConfigDirectory(u, a);
    },
    win32: (u, a) => {
      return _win32ConfigDirectory(u, a);
    }
  };

  return dispatcher[platform](username, appName);
}
/**
 * Return path to users home directory
 * 
 * @param {string} username 
 * @param {string} platform 
 * 
 * @returns {string} path to user home
 */
function getHomeDirectory(username, platform = os.platform()) {
  let dispatcher = {
    darwin: (u) => {
      return _darwinHomeDirectory(u);
    },
    linux: (u) => {
      return _linuxHomeDirectory(u);
    },
    win32: (u) => {
      return _win32HomeDirectory(u);
    }
  };

  return dispatcher[platform](username);
}


/**
 * Get users config path on linux
 * 
 * @param {String} username 
 * @returns {String} Users configuration path
 */
function _linuxConfigDirectory(user, appName = defaultAppDirName) {

  let fakehomedir = _linuxHomeDirectory(user);

  return process.env.XDG_CONFIG_HOME ?
    path.posix.join(process.env.XDG_CONFIG_HOME, kbcPath('linux', appName)) :
    path.posix.join(fakehomedir, '.config', kbcPath('linux', appName));
}

/**
 * Return users home directory on linux
 * 
 * @param {String} user
 * @param {String} usernameFallback - by default set to os.userInfo().username result
 * @returns {String} users home directory
 */
function _linuxHomeDirectory(user, usernameFallback = os.userInfo().username) {

  if (user === null) {
    return _linuxHomeDirectoryPlain();
  }

  let username = user ? user : usernameFallback;

  let fakehomedir = typeof (user) !== 'undefined' ?
    path.posix.join(_linuxHomeDirectoryPlain(), username) :
    os.homedir();

  const splited = fakehomedir.split(path.posix.sep);
  splited.splice(splited.indexOf(usernameFallback), 1, username);
  fakehomedir = path.posix.join(fakehomedir);

  return process.env.XDG_CONFIG_HOME ?
    path.posix.join(process.env.XDG_CONFIG_HOME) :
    fakehomedir;
}

/** just return /home */
function _linuxHomeDirectoryPlain() {
  return path.posix.join(path.posix.sep, 'home');
}

/** guess windows users directory */
function _win32HomeDirectoryPlain(drive) {
  guessedDrive = process.env.SystemDrive || 'C:';
  drive ? drive : guessedDrive;

  return drive ? path.win32.join(drive, 'Users') : path.win32.join(guessedDrive, 'Users');
}

/**
 * Return users home directory on windows
 * 
 * @param {String} user
 * @param {String} usernameFallback - by default set to os.userInfo().username result
 * @returns {String} users home directory
 */
function _win32HomeDirectory(user, usernameFallback = os.userInfo().username) {
  if (user === null) {
    return _win32HomeDirectoryPlain();
  }

  if (typeof (user) == 'undefined') {
    return os.homedir();
  }

  // if user is not defined le't return guessed path
  // let drive = process.env.SystemDrive || 'C:';
  let username = user ? user : usernameFallback;
  return path.win32.join(_win32HomeDirectoryPlain(), username);
}

/**
 * Get users config path on windows
 * 
 * @param {String} username 
 * @returns {String} Users configuration path
 */
function _win32ConfigDirectory(username, appName = defaultAppDirName) {
  let homedir = _win32HomeDirectory(username);
  return path.win32.join(homedir, 'AppData', 'Roaming', kbcPath('win32', appName));
}

/**
 * Return users home directory on macos
 * 
 * @param {String} user
 * @param {String} usernameFallback - by default set to os.userInfo().username result
 * @returns {String} users home directory
 */
function _darwinHomeDirectory(user, usernameFallback = os.userInfo().username) {
  if (user === null) {
    return _darwinHomeDirectoryPlain();
  }

  let username = user ? user : usernameFallback;

  let fakehomedir = user ?
    path.posix.join(_darwinHomeDirectoryPlain(), username) :
    os.homedir();

  const splited = fakehomedir.split(path.posix.sep);
  splited.splice(splited.indexOf(usernameFallback), 1, username);
  fakehomedir = path.posix.join(fakehomedir);
  return fakehomedir;
}

/** return /Users */
function _darwinHomeDirectoryPlain() {
  return path.posix.join(path.posix.sep, 'Users');
}

/**
 * Get users config path on macosx
 * 
 * @param {String} username 
 * @returns {String} Users configuration path
 */
function _darwinConfigDirectory(user, appName = defaultAppDirName) {

  let fakehomedir = _darwinHomeDirectory(user);

  return path.posix.join(fakehomedir, 'Library', 'Preferences', kbcPath('darwin', appName));
}
/**
 * Resolve platform - to longer name
 * @param {String} p - platform linux,win32,darwin
 */
function _resolvePlatform(p = os.platform(), platforms = supportedPlatforms) {
  let defaultP = platforms[os.platform()];

  return (typeof (platforms[p]) === 'undefined') ?
    defaultP :
    platforms[p];
}

function _getPath(username = os.userInfo().username, appName = defaultAppDirName,
  platform = os.platform(),
  filename) {

  if (typeof (filename) === 'undefined') {
    throw new TypeError('Undefined filename');
  }

  if (platform === 'win32') {
    return path.win32.join(
      module.exports.getConfigDirectory(username, appName, platform),
      filename);
  } else {
    return path.posix.join(
      module.exports.getConfigDirectory(username, appName, platform),
      filename);
  }

}

/**
 * Returns path to settings file
 * 
 * @param {String} username 
 * @param {String} platform 
 * @param {string} filename  - config file for settings
 */
function getSettingsPath(
  username = os.userInfo().username,
  appName = defaultAppDirName,
  platform = os.platform(),
  filename = 'settings.conf') {

  return _getPath(username, appName, platform, filename);
}
/**
 * Returns path to client config file
 * 
 * @param {String} username 
 * @param {String} platform 
 * @param {string} filename  -  config file
 */
function getConfigPath(
  username = os.userInfo().username,
  appName = defaultAppDirName,
  platform = os.platform(),
  filename = 'client.config') {

  return _getPath(username, appName, platform, filename);
}

/**
 * Returns path to credentials file
 * 
 * @param {String} username 
 * @param {String} platform 
 * @param {string} filename  - config file for credentials
 */
function getCredentialsPath(
  username = os.userInfo().username,
  appName = defaultAppDirName,
  platform = os.platform(),
  filename = 'cred.conf') {

  return _getPath(username, appName, platform, filename);
}

module.exports = {
  getPlatformDirectoryName: getPlatformDirectoryName,
  getHomeDirectory: getHomeDirectory,
  getConfigDirectory: getConfigDirectory,
  getSettingsPath: getSettingsPath,
  getCredentialsPath: getCredentialsPath,
  getConfigPath: getConfigPath,
  __private: {
    _getPath: _getPath,
    _resolvePlatform: _resolvePlatform,
    __kbcPath: kbcPath,
    _win32HomeDirectoryPlain: _win32HomeDirectoryPlain,
    _win32HomeDirectory: _win32HomeDirectory,
    _win32ConfigDirectory: _win32ConfigDirectory,
    _darwinHomeDirectoryPlain: _darwinHomeDirectoryPlain,
    _darwinHomeDirectory: _darwinHomeDirectory,
    _darwinConfigDirectory: _darwinConfigDirectory,
    _linuxHomeDirectoryPlain: _linuxHomeDirectoryPlain,
    _linuxHomeDirectory: _linuxHomeDirectory,
    _linuxConfigDirectory: _linuxConfigDirectory
  }
};