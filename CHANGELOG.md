## [Unreleased]

## [0.1.2] - 2019-02-05
### Fixed
- bugs on darwin platform
- added new tests for getHomeDirectory()

## [0.1.1] - 2019-02-05
### Fixed
- using path.$platform.join - otherwise tests for win32 stuff failed on linux

### Added
- support for homedirectory - getHomeDirectory()

## [0.1.0] - 2018-12-19
### Added
- support for different organizations and product name directory

## [0.0.7] - 2018-06-05
### Added
- getPlatformDirectoryName
- tests

### Fixed
- win32 returning wrong homedir if node ran as NT Authority\SYSTEM

## [0.0.6] - 2018-05-25
### Fixed
- Various fixes for tests on windows

## [0.0.5] - 2018-05-15
### Added
- 100% test coverage

## [0.0.4] - 2018-05-15
### Added
- various new functions for detecting paths for kbc

## [0.0.3] - 2017-03-13
### Fixed
- broken test

## [0.0.2] - 2017-03-13
### Changed
- fixed package.json

## [0.0.1] - 2018-03-13
- initial release
