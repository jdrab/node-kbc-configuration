const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;
const os = require('os');
const path = require('path');

const kbc = require('../lib/index.js');
//==============================================================================

const username = 'testuser';
const darwinDefaultCompany = 'com.somi.sk';
const defaultAppName = 'Kerber Backup Client';
const linuxAppConfDirName = defaultAppName;
const win32AppConfDirName = defaultAppName;
const darwinAppConfDirName = path.posix.join(darwinDefaultCompany, defaultAppName);
const linuxHomeDir = path.posix.sep + 'home';
const linuxConfigPath = path.posix.join(linuxHomeDir, username, '.config');

const linuxConfigPathNoUsername =
  path.posix.join(path.posix.sep, 'home', os.userInfo()
    .username, '.config', defaultAppName);


const customXDGConfigHome =
  path.posix.join(path.posix.sep, 'home', username, '.my_config');

const darwinHomeDir = path.posix.sep + 'Users';
const darwinConfigPath =
  path.posix.join(darwinHomeDir, username, 'Library', 'Preferences');


const win32UsersDir = path.win32.join('C:', 'Users');
const win32CustomizedSystemDrive = 'D:';
const win32CustomizedUsersDir = path.win32.join(win32CustomizedSystemDrive, 'Users');
const win32ConfigPath =
  path.win32.join(win32UsersDir, username, 'AppData', 'Roaming');

const win32CustomeizedAppData =
  path.win32.join(win32CustomizedSystemDrive, 'Users', username, 'AppData', 'Roaming');

const win32SettingsPath = path.win32.join(win32ConfigPath, win32AppConfDirName, 'settings.conf');
const linuxSettingsPath = path.posix.join(linuxConfigPath, linuxAppConfDirName, 'settings.conf');
const darwinSettingsPath = path.posix.join(darwinConfigPath, darwinAppConfDirName, 'settings.conf');

const win32CredentialsPath = path.win32.join(win32ConfigPath, win32AppConfDirName, 'cred.conf');
const linuxCredentialsPath = path.posix.join(linuxConfigPath, linuxAppConfDirName, 'cred.conf');
const darwinCredentialsPath = path.posix.join(darwinConfigPath, darwinAppConfDirName, 'cred.conf');

const win32ConfigFilePath = path.win32.join(win32ConfigPath, win32AppConfDirName, 'client.config');
const linuxConfigFilePath = path.posix.join(linuxConfigPath, linuxAppConfDirName, 'client.config');
const darwinConfigFilePath = path.posix.join(darwinConfigPath, darwinAppConfDirName, 'client.config');



//==============================================================================

describe('_resolvePlatform', () => {
  it('returning default resolved platform', () => {
    if (process.platform === 'linux') {
      assert.equal(kbc.__private._resolvePlatform('unsupported'), 'Linux');
    }
    if (process.platform === 'win32') {
      assert.equal(kbc.__private._resolvePlatform('unsupported'), 'Windows');
    }
    if (process.platform === 'darwin') {
      assert.equal(kbc.__private._resolvePlatform('unsupported'), 'macOS');
    }
  });
  it('returning resolved platform', () => {
    assert.equal(kbc.__private._resolvePlatform('linux'), 'Linux');
    assert.equal(kbc.__private._resolvePlatform('win32'), 'Windows');
    assert.equal(kbc.__private._resolvePlatform('darwin'), 'macOS');
  });

});

describe('getHomeDirectory', () => {

  it(`linux - should return /home/${username}`, () => {
    assert.equal(kbc.getHomeDirectory(username, 'linux'), `/home/${username}`);
  });

  // if username is not defined, we're using os.homedir(), so this can not be tested on other plaforms
  if (process.platform === 'linux') {
    it(`linux - undefined username - should return /home/${os.userInfo().username}`, () => {
      assert.equal(kbc.getHomeDirectory(undefined, 'linux'), `/home/${os.userInfo().username}`);
    });
  }

  it(`darwin - should return /Users/${username}`, () => {
    assert.equal(kbc.getHomeDirectory(username, 'darwin'), `/Users/${username}`);
  });

  // if username is not defined, we're using os.homedir(), so this can not be tested on other plaforms
  if (process.platform === 'darwin') {
    it(`darwin - undefined username - should return /Users/${username}`, () => {
      assert.equal(kbc.getHomeDirectory(undefined, 'darwin'), `/Users/${os.userInfo().username}`);
    });
  }

  if (process.platform === 'win32') {
    it(`XXX windows - get users defautl homedir - username is undefined - should return ${os.homedir()} value`, () => {
      assert.equal(kbc.getHomeDirectory(undefined, 'win32'), os.homedir());
    });
  }

  // this test depends on modified process.env.SystemDrive 
  // if (os.platform() === 'win32') {
  it(`ABC with username win32 - should return ${path.win32.join(win32UsersDir, username)}`, () => {
    assert.equal(kbc.getHomeDirectory(username, 'win32'), path.win32.join(win32UsersDir, username));
  });
  // }

});

describe('getPlatformDirectoryName', () => {
  //linux - should return: kerber-backup-client:
  it(`linux - should return: ${linuxAppConfDirName}`, () => {
    assert.equal(kbc.getPlatformDirectoryName('linux', defaultAppName), linuxAppConfDirName);
  });

  it(`win32 - should return: ${win32AppConfDirName}`, () => {
    assert.equal(kbc.getPlatformDirectoryName('win32', defaultAppName), win32AppConfDirName);
  });

  it(`darwin - should return: ${darwinAppConfDirName}`, () => {
    assert.equal(kbc.getPlatformDirectoryName('darwin', darwinDefaultCompany, defaultAppName), darwinAppConfDirName);
  });
});

describe('_getPath - TypeError', function () {
  it('should throw TypeError', function () {
    expect(function () {
      kbc.__private._getPath();
    }).to.throw();
  });
});

describe('getConfigPath', function () {
  // if (os.platform() === 'linux') {
  describe('platform - linux', function () {
    it('should return ' + linuxConfigFilePath,
      function () {
        assert.equal(
          linuxConfigFilePath,
          kbc.getConfigPath(username, defaultAppName, 'linux')
        );
      });
  });
  // }
  // if (os.platform() === 'win32') {
  describe('platform - windows', function () {
    it('should return ' + win32ConfigFilePath,
      function () {
        assert.equal(win32ConfigFilePath,
          kbc.getConfigPath(username, defaultAppName, 'win32')
        );
      });
  });
  // }
  describe('platform - darwin', function () {
    it('should return ' + darwinConfigFilePath,
      function () {
        assert.equal(
          darwinConfigFilePath,
          kbc.getConfigPath(username, defaultAppName, 'darwin')
        );
      });
  });
});

describe('getSettingsPath', function () {
  if (os.platform() === 'linux') {
    describe('platform - linux', function () {
      it('should return ' + linuxSettingsPath, function () {
        assert.equal(
          linuxSettingsPath,
          kbc.getSettingsPath(username, defaultAppName, 'linux'));
      });
    });
  }
  if (os.platform() === 'win32') {
    describe('platform - windows', function () {
      it('should return ' + win32SettingsPath,
        function () {
          assert.equal(
            win32SettingsPath,
            kbc.getSettingsPath(username, defaultAppName, 'win32')
          );
        });
    });
  }
  describe('platform - darwin', function () {
    it('should return ' + darwinSettingsPath,
      function () {
        assert.equal(
          darwinSettingsPath,
          kbc.getSettingsPath(username, defaultAppName, 'darwin')
        );
      });
  });
});

describe('getCredentialsPath', function () {
  if (os.platform() === 'linux') {
    describe('platform - linux', function () {
      it('should return ' + linuxCredentialsPath, function () {
        assert.equal(linuxCredentialsPath,
          kbc.getCredentialsPath(username, defaultAppName, 'linux'));
      });
    });
  }
  if (os.platform() === 'win32') {
    describe('platform - windows', function () {
      it('should return ' + win32CredentialsPath,
        function () {
          assert.equal(
            win32CredentialsPath,
            kbc.getCredentialsPath(username, defaultAppName, 'win32')
          );
        });
    });
  }
  describe('platform - darwin', function () {
    it('should return ' + darwinCredentialsPath,
      function () {
        assert.equal(
          darwinCredentialsPath,
          kbc.getCredentialsPath(username, defaultAppName, 'darwin')
        );
      });
  });
});



describe('platform - win32 home - username is null', function () {
  it('should return ' + win32UsersDir, function () {
    assert.equal(kbc.getHomeDirectory(null, 'win32'), win32UsersDir);
  });
});
describe('platform - linux home - username is null', function () {
  it('should return ' + linuxHomeDir, function () {
    assert.equal(kbc.getHomeDirectory(null, 'linux'), linuxHomeDir);
  });
});
describe('platform - darwin home - username is null', function () {
  it('should return ' + darwinHomeDir, function () {
    assert.equal(kbc.getHomeDirectory(null, 'darwin'), darwinHomeDir);
  });
});

describe('getConfigDirectory', function () {
  if (os.platform() === 'linux') {
    describe('platform - linux', function () {
      it('should return ' + linuxConfigPath, function () {
        assert.equal(kbc.getConfigDirectory(username, defaultAppName, 'linux'),
          path.posix.join(linuxConfigPath, linuxAppConfDirName));
      });
    });
    describe('platform - linux - with defined XDG_CONFIG_HOME', function () {
      it('should return ' + path.posix.join(customXDGConfigHome, linuxAppConfDirName), function () {
        process.env.XDG_CONFIG_HOME = customXDGConfigHome;

        assert.equal(
          kbc.getConfigDirectory(username, defaultAppName, 'linux'),
          path.posix.join(customXDGConfigHome, linuxAppConfDirName)
        );
        delete process.env.XDG_CONFIG_HOME;
      });
    });

    describe('platform - linux withouth username', function () {
      it('should return ' + linuxConfigPathNoUsername, function () {
        assert.equal(kbc.getConfigDirectory(false, defaultAppName, 'linux'), linuxConfigPathNoUsername);
      });
    });
  }

  /* na macu v ~/Library/Preferences a asi com.somi.kerber-backup-client */
  describe('platform - darwin', function () {
    it('should return ' + path.posix.join(darwinConfigPath, darwinAppConfDirName), function () {
      assert.equal(
        kbc.getConfigDirectory(username, defaultAppName, 'darwin'),
        path.posix.join(darwinConfigPath, darwinAppConfDirName)
      );
    });
  });

  if (os.platform() === 'win32') {
    /* windows test v %APPDATA%/Kerber Backup Client ma byt configdir */
    describe('platform - win32', function () {
      it('should return ' + path.win32.join(win32ConfigPath, win32AppConfDirName), function () {
        assert.equal(
          kbc.getConfigDirectory(username, defaultAppName, 'win32'),
          path.win32.join(win32ConfigPath, win32AppConfDirName)
        );
      });
    });

    describe('platform - win32 with custom AppData directory', function () {
      it('should return ' + path.win32.join(win32CustomeizedAppData, win32AppConfDirName), function () {
        const previous_appdata = process.env.APPDATA;
        const previous_system_drive = process.env.SystemDrive;
        process.env.APPDATA = win32CustomeizedAppData;
        process.env.SystemDrive = win32CustomizedSystemDrive;
        assert.equal(
          kbc.getConfigDirectory(username, defaultAppName, 'win32'),
          path.win32.join(win32CustomeizedAppData, win32AppConfDirName)
        );
        process.env.APPDATA = previous_appdata;
        process.env.SystemDrive = previous_system_drive;
      });
    });
  }
});